#--------
# GENERAL
# -------
update_auth_dns: False

# base for the swagger docs
api_server_base: api.araport.org

# ----
# APIM
# ----
tenant_id: araport-org
agave_env: prod
host: api.araport.org
# host: araport-org.tenants.prod.agaveapi.co
tenant_admin_role: Internal/araport-org-services-admin

# the url registered in APIM for the profiles API. For tenants using the django service, the
# profiles container will have role: profiles.araport-org.agave.tacc.utexas.edu
# For tenants like ldap, the URL will need to point to a different service (e.g. the PHP service).
agave_profiles_url: profiles.araport-org.agave.tacc.utexas.edu/profiles

deploy_admin_password_grant: True
access_token_validity_time: 14400
deploy_custom_oauth_app: True
update_apim_core_dns: False
apim_increase_global_timeout: False


# -----
# MYSQL
# -----
mysql_host: db.prod.agaveapi.co
#mysql_port: 3301
mysql_port: 3306

# ------------------
# IDENTITY & CLIENTS
# ------------------

# whether or not to deploy the agave_id service container.
use_hosted_id: True

# When true, the services will not make any updates.
agave_id_read_only: False

# unigue id for the "domain name" of the userstore in APIM -- only impacts file name and userstore id in APIM.
# does NOT impact the search base.
hosted_id_domain_name: araport

# URL or service discovery token for the hosted LDAP instance (including port)
ldap_name: ldaps://agaveldap.tacc.utexas.edu:636

# account to bind to the LDAP db
auth_ldap_bind_dn: cn=Manager,o=agaveapi

# base search directory for user accounts
ldap_base_search_dn: o=agaveapi

# Whether or not to check the JWT; When this is False, certain features will not be available such as the
# "me" lookup feature since these features rely on profile information in the JWT.
agave_id_check_jwt: True

# Actual header name that will show up in request.META; value depends on APIM configuration, in particular
# the tenant id specified in api-manager.xml
jwt_header: HTTP_ASSERTION

# Absolute path to the public key of the APIM instance; used for verifying the signature of the JWT.
agave_id_apim_pub_key: /home/apim/public_keys/apim_default.pub

# APIM Role required to make updates to the LDAP database
agave_id_user_admin_role: Internal/user-account-manager

# Whether or not the USER_ADMIN_ROLE is checked before allowing updates to the LDAP db (/users service)
agave_id_check_user_admin_role: True

# Set USE_CUSTOM_LDAP = True to use a database with a different schema than the traditional Agave ldap (e.g. TACC
# tenant). Some specific fields will still be required, for example the uid field as the primary key.
use_custom_ldap: False

# Base URL of this instance of the service. Used to populate the hyperlinks in the responses.
agave_id_app_base: https://api.araport.org

# DEBUG = True turns up logging and causes Django to generate excpetion pages with stack traces and
# additional information. Should be False in production.
# Updated -- 8/2015: Due to a bug in django, we currently set this to true so that the ALLOWED_HOSTS filtering is
# not activated.
agave_id_debug: True

# Beanstalk connection info
beanstalk_server: 129.114.97.131
beanstalk_port: 11300
beanstalk_tube: prod
beanstalk_srv_code: 0001-001
tenant_uuid: 0001411570898814

# list of additional APIs to automatically subscribe clients to
agave_clients_additional_apis:
    - name: Adama
      version: v0.3
      provider: admin
    - name: ThalemineNew
      version: 0.1.0
      provider: admin
# -------------

# These settings are only used when deploying the account sign up web application:
# SMTP - used for the email loop account verification:
# mail_server:
# mail_server_port:
# email_base_url:


# ----
# CORE
# ----
# http or https, depending on whether auth is deployed in the tacc dc
core_api_protocol: http

# IP of core host. needed until dns gets resolved
core_host: 129.114.97.130


# -----
# HTTPD
# -----
# cert file - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
#cert_file: star.araport.org.crt

# cert key file - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
#cert_key_file: star.araport.org.key

# add when mounting in a CA cert (not used for self-signed certs) - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
#ssl_ca_cert_file: root.crt

# add when mounting in a CA chain (not used for self-signed certs) - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
#ssl_ca_chain_file: chain.crt

virtualhosts:
#    - server_name: api.araport.org
#      base_cert_path: aip-vanity
#      cert_file: star.araport.org.crt
#      cert_key_file: star.araport.org.key
#      ssl_ca_cert_file: root.crt
#      ssl_ca_chain_file: chain.crt
    - server_name: api.araport.org
      base_cert_path: aip-vanity
      cert_file: api.araport.org.crt
      cert_key_file: api.araport.org.key
      ssl_ca_cert_file: api.araport.org.ca.crt
    - server_name: araport-org.tenants.prod.agaveapi.co
      base_cert_path: agaveapi
      cert_file: __tenants_prod_agaveapi_co_cert.cer
      cert_key_file: _.tenants.prod.agaveapi.co.2015-11-11.key
      ssl_ca_cert_file: __tenants_prod_agaveapi_co_interm.cer


# --------
# HA Proxy
# --------
ha_deployment: True
hap_servers:
    - name: auth1a
      ip: 172.17.0.1
      port: 4080
      ssl_port: 40443
    - name: auth1b
      ip: 172.17.42.1
      port: 4080
      ssl_port: 40443
    - name: auth2a
      ip: 172.17.0.1
      port: 5080
      ssl_port: 50443
    - name: auth2b
      ip: 172.17.42.1
      port: 5080
      ssl_port: 50443

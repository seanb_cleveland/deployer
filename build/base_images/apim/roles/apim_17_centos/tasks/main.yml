---
- name: ensure sudo installed
  yum: name=sudo state=present

- name: ensure libselinux-python installed
  yum: name=libselinux-python state=present
  sudo: yes

- name: Add user apim
  user: name=apim generate_ssh_key=yes state=present
  sudo: yes

- name: Add user apim to sudoers with no password
  lineinfile:
    dest: /etc/sudoers.d/99-apim-user
    line: 'apim ALL=(ALL) NOPASSWD:ALL'
    state: present
    create: yes
  sudo: yes

- name: upload the default key
  copy: src=apim.priv.key dest=/home/apim/.ssh/apim mode=0600
  sudo: yes
  sudo_user: apim

- name: make apim the default key
  user: name=apim ssh_key_file=/home/apim/.ssh/apim state=present
  sudo: yes

- name: add default authorized key
  authorized_key: user=apim key="{{ item }}"
  sudo: yes
  sudo_user: apim
  with_file:
    - apim.pub

- name: Ensure permissions on apim sudoers
  file:
    path: /etc/sudoers.d/99-apim-user
    mode: 0440
  sudo: yes

- name: add tmp/hosts template file
  copy: src=hosts.j2 dest=/tmp/hosts
  sudo: yes

- name: give other users (e.g. apache) access to apim home directory
  file:
    path: /home/apim
    mode: 0755
  sudo: yes

- name: Install git
  yum: name=git state=present
  sudo: yes

- name: install crontab
  yum: name=cronie state=present
  sudo: yes

- name: install unzip
  yum: name=unzip state=present
  sudo: yes

- name: copy logrotate.conf.j2
  copy: src=apim/logrotate.conf.j2 dest=/etc/logrotate.conf
  sudo: yes

- name: install wget
  yum: name=wget state=present
  sudo: yes

- name: get pip
  get_url: url=https://bootstrap.pypa.io/get-pip.py dest=/get-pip.py
  sudo: yes

- name: install pip
  command: python /get-pip.py
  sudo: yes

- name: ensure /values.yml is present
  file: path=/values.yml state=touch
  sudo: yes

- name: ensure /passwords is present
  file: path=/passwords state=touch
  sudo: yes

- name: add template_compiler
  copy: src=template_compiler/ dest=/template_compiler
  sudo: yes

- name: pip install template_compiler requirements
  pip: requirements=/template_compiler/requirements.txt
  sudo: yes

- name: ensure permissions on the template_entry.sh
  file: path=/template_compiler/template_entry.sh  mode=744
  sudo: yes

- name: copy templates file
  copy: src=templates dest=/templates
  sudo: yes

- name: Create install directory
  file: path=/home/apim/install_files/java/ state=directory
  sudo: yes
  sudo_user: apim

- name: Download Java 7 installed
  command: "curl -LO 'http://download.oracle.com/otn-pub/java/jdk/7u51-b13/jdk-7u51-linux-x64.rpm' -H 'Cookie: oraclelicense=accept-securebackup-cookie'"
  args:
    chdir: /home/apim/install_files/java/
    creates: jdk-7u51-linux-x64.rpm
  sudo: yes
  sudo_user: apim

- name: Install java 7
  yum: name=/home/apim/install_files/java/jdk-7u51-linux-x64.rpm state=present
  sudo: yes

- name: ensure apim directory present
  file: path=/home/apim/install_files/apim state=directory
  sudo: yes
  sudo_user: apim

- name: copy the apim installation files
  copy: src=apim/ dest=/home/apim/install_files/apim/
  sudo: yes
  sudo_user: apim

- name: unpack apim stock
  command: unzip /home/apim/install_files/apim/wso2am-1.7.0.zip -d /home/apim creates=/home/apim/wso2am-1.7.0
  sudo: yes
  sudo_user: apim

- name: copy components/lib dir
  copy: src=apim/components/lib/ dest=/home/apim/wso2am-1.7.0/repository/components/lib
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: copy APIs dir
  copy: src=apim/APIs/v2_dns/ dest=/home/apim/wso2am-1.7.0/repository/deployment/server/synapse-configs/default/api/
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: copy catalina-server.xml
  copy: src=apim/catalina-server.xml dest=/home/apim/wso2am-1.7.0/repository/conf/tomcat/
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: copy client-truststore
  copy: src=apim/client-truststore-agaveldap01.jks dest=/home/apim/wso2am-1.7.0/repository/resources/security/client-truststore.jks
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: copy api-manager.xml.j2
  copy: src=apim/api-manager.xml.j2 dest=/home/apim/wso2am-1.7.0/repository/conf/api-manager.xml
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: copy carbon.xml.j2
  copy: src=apim/carbon.xml.j2 dest=/home/apim/wso2am-1.7.0/repository/conf/carbon.xml
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: copy master-datasources.xml.j2
  copy: src=apim/master-datasources.xml.j2 dest=/home/apim/wso2am-1.7.0/repository/conf/datasources/master-datasources.xml
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: create userstores directory
  file: path=/home/apim/wso2am-1.7.0/repository/deployment/server/userstores/ state=directory
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: copy hosted identity-userstore.xml.j2
  copy: src=apim/hosted-identity-userstore.xml.j2 dest=/home/apim/wso2am-1.7.0/repository/deployment/server/userstores/hosted_id_domain_name.xml
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: set up remote userstore
  copy: src=apim/remote-userstore.xml.j2 dest=/home/apim/wso2am-1.7.0/repository/deployment/server/userstores/remote_domain_name.xml
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: copy apim init.d script
  copy: src=apim/apim_initd.j2 dest=/etc/init.d/apim mode=0775
  sudo: yes
  tags:
    - initial

- name: ensure nohup.out is present
  file: path=/home/apim/wso2am-1.7.0/nohup.out state=touch
  sudo: yes
  sudo_user: apim

- name: Ensure backups directory present
  file: path=/home/apim/backups state=directory
  sudo: yes
  sudo_user: apim

- name: copy entry.sh
  copy: src=entry.sh dest=/home/apim/entry.sh
  sudo: yes
  sudo_user: apim
  tags:
    - initial

- name: copy ldap.conf
  copy: src=ldap.conf dest=/etc/openldap/ldap.conf
  sudo: yes
  tags:
    - initial
